export const colors = {
  main: '#ff9000',
  secondary: '#28262e',
  white: '#f4ede8',
  orange: '#d63702',
  grey: '#232129',
};
