import { callApi } from './api';

type Input = {
  email: string;
  password: string;
  fullName: string;
};

export async function CreateUserService(input: Input): Promise<any> {
  return callApi({
    method: 'POST',
    url: '/user',
    apiData: {
      fullName: input.fullName,
      email: input.email,
      password: input.password,
    },
  });
}
