import { callApi } from './api';

type Input = {
  email: string;
  password: string;
};

export async function SignInService({ email, password }: Input): Promise<any> {
  return callApi({
    method: 'POST',
    url: '/auth/signin',
    apiData: {
      email,
      password,
    },
  });
}
