import axios, { AxiosRequestConfig } from 'axios';

const uri = 'http://localhost:3000/api/v1';

type InputData = AxiosRequestConfig & {
  apiData?: any;
};

export const callApi = ({
  method,
  url,
  apiData,
  headers,
  params,
}: InputData): Promise<any> => {
  return axios({
    baseURL: uri,
    method,
    url,
    params,
    data: apiData,
    headers,
  });
};
