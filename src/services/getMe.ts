import { callApi } from './api';

export async function GetLoggedUserService(
  authorization: string,
): Promise<any> {
  return callApi({
    method: 'GET',
    url: '/user/me',
    headers: {
      authorization,
    },
  });
}
