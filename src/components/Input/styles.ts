import styled, { css } from 'styled-components';

import { colors } from '../../styles/colors';
import Tooltip from '../Tooltip';

interface ContainerProps {
  isFocused: boolean;
  isFilled: boolean;
  isErrored: boolean;
}

export const Container = styled.div<ContainerProps>`
  background: ${colors.grey};
  border-radius: 10px;
  padding: 16px;
  width: 100%;

  border: 2px solid #232129;
  color: #666660;

  display: flex;
  align-items: center;

  & + div {
    margin-top: 8px;
  }

  ${(props: ContainerProps) =>
    props.isErrored &&
    css`
      border-color: ${colors.orange};
    `}

  ${(props: ContainerProps) =>
    props.isFocused &&
    css`
      border-color: ${colors.main};
      color: ${colors.main};
    `}

  ${(props: ContainerProps) =>
    props.isFilled &&
    css`
      color: ${colors.main};
    `}

  input {
    background: transparent;
    flex: 1;
    border: 0;
    color: ${colors.white};

    &::placeholder {
      color: #666660;
    }
  }

  svg {
    margin-right: 16px;
  }
`;

export const Error = styled(Tooltip)`
  height: 20px;
  margin-left: 16px;

  svg {
    margin: 0;
  }

  span {
    background: ${colors.orange};
    color: #fff;

    &::before {
      border-color: ${colors.orange} transparent;
    }
  }
`;
