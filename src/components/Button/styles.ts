import { shade } from 'polished';
import styled, { css } from 'styled-components';
import { colors } from '../../styles/colors';

type ContainerProps = {
  disabled?: boolean;
};

export const Container = styled.button<ContainerProps>`
  background: ${colors.secondary};
  border-radius: 10px;
  border: 0;
  padding: 0 16px;
  width: 100%;
  height: 56px;
  color: #312e38;
  font-weight: 700;
  margin-top: 16px;
  transition: background-color 0.2s;
  background: ${colors.main};

  ${(props) =>
    props.disabled &&
    css`
      color: #fff;
      pointer-events: none;
      background: ${colors.orange};
    `}

  &:hover {
    background: ${(props) => !props.disabled && shade(0.2, colors.main)};
  }
`;
