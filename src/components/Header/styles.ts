import styled from 'styled-components';
import { colors } from '../../styles/colors';

export const Container = styled.header`
  padding: 32px 0;
  background: ${colors.secondary};

  @media (max-width: 768px) {
    padding: 32px;
  }
`;

export const HeaderContent = styled.div`
  max-width: 1120px;
  margin: 0 auto;
  display: flex;
  align-items: center;
  justify-content: space-between;

  > div {
    display: flex;
    align-items: center;
    justify-content: space-between;
    text-align: center;

    > p {
      margin-right: 15px;
    }
  }

  > img {
    height: 60px;

    @media (max-width: 768px) {
      height: 40px;
    }
  }

  button {
    margin-left: auto;
    background: transparent;
    border: 0;

    svg {
      color: ${colors.main};
      width: 20px;
    }
  }
`;
