import React from 'react';
import { FiPower } from 'react-icons/fi';
import { useHistory } from 'react-router-dom';
import logoImg from '../../assets/logo.png';
import { useAuth } from '../../hooks/auth';
import { useToast } from '../../hooks/toast';
import useAppWebSocket from '../../hooks/websocket';
import { Container, HeaderContent } from './styles';

export const Header: React.FC = () => {
  const { addToast } = useToast();
  const history = useHistory();
  const { user, token, signOut } = useAuth();

  const sendMessage = useAppWebSocket({
    onMessage: (messageEvent: any) => {
      if (messageEvent.data === 'ping') {
        sendMessage('pong');
      }

      if (messageEvent?.data === 'SafePageReleased') {
        addToast({
          type: 'info',
          title: 'Mensagem do servidor',
          description: 'Você saiu da página segura',
        });
        history.push('dashboard');
      }
    },
  });

  const handleSignOut = () => {
    const payload = {
      event: 'userExitedOnSafePage',
      data: {
        authorization: token,
      },
    };

    sendMessage(JSON.stringify(payload));
    setTimeout(() => {
      signOut();
    }, 2000);
  };

  return (
    <Container>
      <HeaderContent>
        <img src={logoImg} alt="Hubbe" />
        <div>
          {user.email && <p>{user.email}</p>}
          <button type="button" onClick={handleSignOut}>
            <FiPower size={20} />
          </button>
        </div>
      </HeaderContent>
    </Container>
  );
};
