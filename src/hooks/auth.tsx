import React, { createContext, useCallback, useContext, useState } from 'react';
import { SignInService } from '../services/auth';
import { GetLoggedUserService } from '../services/getMe';

interface User {
  fullName: string;
  email: string;
}

interface AuthState {
  token: string;
  user: User;
}

interface SignInCredentials {
  email: string;
  password: string;
}

interface AuthContextData {
  user: User;
  token: string;
  signIn(credentials: SignInCredentials): Promise<void>;
  signOut(): void;
  updateUser(user: User): void;
}

const AuthContext = createContext<AuthContextData>({} as AuthContextData);

export const AuthProvider: React.FC = ({ children }) => {
  const [data, setData] = useState<AuthState>(() => {
    const token = localStorage.getItem('@Hubbe:token');
    const user = localStorage.getItem('@Hubbe:user');

    if (token && user) {
      return { token, user: JSON.parse(user) };
    }

    return {} as AuthState;
  });

  const signIn = useCallback(async ({ email, password }) => {
    localStorage.removeItem('@Hubbe:token');
    localStorage.removeItem('@Hubbe:user');
    const signin = await SignInService({ email, password });

    const { accessToken: token } = signin.data.data;

    const getUser = await GetLoggedUserService(token);
    const user = getUser?.data?.data;

    localStorage.setItem('@Hubbe:token', token);
    localStorage.setItem('@Hubbe:user', JSON.stringify(user));

    setData({ token, user });
  }, []);

  const signOut = useCallback(() => {
    localStorage.removeItem('@Hubbe:token');
    localStorage.removeItem('@Hubbe:user');

    setData({} as AuthState);
  }, []);

  const updateUser = useCallback(
    (user: User) => {
      localStorage.setItem('@Hubbe:user', JSON.stringify(user));

      setData({
        token: data.token,
        user,
      });
    },
    [setData, data.token],
  );

  return (
    <AuthContext.Provider
      value={{
        user: data.user,
        signIn,
        signOut,
        updateUser,
        token: data.token,
      }}>
      {children}
    </AuthContext.Provider>
  );
};

export const useAuth = (): AuthContextData => {
  const context = useContext(AuthContext);

  if (!context) {
    throw new Error('useAuth must be user within an AuthProvider');
  }

  return context;
};
