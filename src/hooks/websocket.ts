import useWebSocket from 'react-use-websocket';

import { useEffect } from 'react';
import { env } from '../env';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
const useAppWebSocket = ({ onMessage }: any) => {
  const { sendMessage } = useWebSocket(env.socketUri, {
    onOpen: () => console.log(`Connected to App WS`),
    onMessage: (messageEvent) => {
      if (onMessage) {
        onMessage(messageEvent, sendMessage);
      }
    },
    onError: (event) => {
      console.error(event);
    },
    shouldReconnect: () => true,
    reconnectInterval: 3000,
  });

  useEffect(() => {
    console.log('SOCKET');
  }, []);

  return sendMessage;
};

export default useAppWebSocket;
