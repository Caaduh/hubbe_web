import React, { useEffect, useState } from 'react';

import { useHistory } from 'react-router-dom';
import { Container, Content, InternalBox } from './styles';

import Button from '../../components/Button';
import { Header } from '../../components/Header';
import { useAuth } from '../../hooks/auth';
import { useToast } from '../../hooks/toast';
import useAppWebSocket from '../../hooks/websocket';

const Dashboard: React.FC = () => {
  const { addToast } = useToast();
  const history = useHistory();
  const { token, user } = useAuth();
  const [safeRoomFull, setSafeRoomFull] = useState(false);

  const sendMessage = useAppWebSocket({
    onMessage: (messageEvent: any) => {
      if (messageEvent?.data === 'SafePageReleased') {
        setSafeRoomFull(false);
        history.push('dashboard');
      }
      if (messageEvent?.data === 'SafePageBusy') {
        setSafeRoomFull(true);
      }
      if (messageEvent?.data === 'SafePageFree') {
        setSafeRoomFull(false);
      }
      if (messageEvent?.data === 'ConnectedSafePage') {
        setSafeRoomFull(true);
        history.push('safe-page');
        addToast({
          type: 'info',
          title: 'Mensagem do servidor',
          description: messageEvent.data,
        });
      }
    },
  });

  useEffect(() => {
    const payload = {
      event: 'verifySafePageStatus',
      data: {
        authorization: token,
      },
    };

    sendMessage(JSON.stringify(payload));
  }, [safeRoomFull]);

  const handleEnterSafeRoom = () => {
    const payload = {
      event: 'userLoggedOnSafePage',
      data: {
        authorization: token,
      },
    };
    sendMessage(JSON.stringify(payload));
  };

  return (
    <Container>
      <Header />

      <Content>
        <InternalBox>
          {user?.fullName && <p>Bem vindo(a) - {user?.fullName}</p>}

          <Button disabled={safeRoomFull} onClick={handleEnterSafeRoom}>
            {safeRoomFull
              ? 'Sala segura está ocupada'
              : 'Entrar na sala segura'}
          </Button>
        </InternalBox>
      </Content>
    </Container>
  );
};

export default Dashboard;
