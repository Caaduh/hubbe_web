import React from 'react';
import { useHistory } from 'react-router-dom';
import { useToast } from '../../hooks/toast';

import Button from '../../components/Button';
import { Header } from '../../components/Header';
import { useAuth } from '../../hooks/auth';
import useAppWebSocket from '../../hooks/websocket';
import { Container, Content } from './styles';

const SafePage: React.FC = () => {
  const { addToast } = useToast();
  const history = useHistory();
  const { user, token } = useAuth();

  const sendMessage = useAppWebSocket({
    onMessage: (messageEvent: any) => {
      if (messageEvent.data === 'ping') {
        sendMessage('pong');
      }

      if (messageEvent?.data === 'SafePageReleased') {
        addToast({
          type: 'info',
          title: 'Mensagem do servidor',
          description: 'Você saiu da página segura',
        });
        history.push('dashboard');
      }
    },
  });

  const handleBackToDashboard = () => {
    const payload = {
      event: 'userExitedOnSafePage',
      data: {
        authorization: token,
      },
    };

    sendMessage(JSON.stringify(payload));
  };

  return (
    <Container>
      <Header />
      <Content>
        <h1>Bem vindo(a) a sala segura {user?.fullName}</h1>
        <Button onClick={handleBackToDashboard}>Sair da sala segura</Button>
      </Content>
    </Container>
  );
};

export default SafePage;
