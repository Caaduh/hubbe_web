import styled from 'styled-components';

export const Container = styled.div``;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin: 30px auto;
  max-width: 1120px;
  width: 100%;

  @media (max-width: 768px) {
    margin: 10px auto;
    max-width: 420px;

    h1 {
      font-size: 22px;
      text-align: center;
    }
  }
`;
