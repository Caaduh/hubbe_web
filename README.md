<h1 align="center">🚀 You are not alone 🚀</h1>

## Author

### Made with ❤️ by Eduardo Alves 👋 [@codespoa](https://github.com/codespoa)

This project was built to be very simple, basically being able to enter a safe room and if someone is already there it will change the status to busy, you can also create users and log into the application.

![app](image.png)
![app](image-1.png)
![app](image-2.png)
![app](image-3.png)

`For this project we will use fio, but if you prefer npm just change the command`

### Techs
- [x] [React.js]
- [x] [WS]
- [x] [JWT]
- [x] [ESlint]


## Requirements

- Node.js version: v16.14.2

## Getting Started

Clone this repository

```bash
git clone 
```
Access the project folder

```bash
cd hubbe_web
```

## Dependencies

Install all dependencies for the project

```sh
yarn
```

## Starting the Application

For dev mode, after all environment variables are configured, run

```sh
yarn start
```

your application its running on:

```sh
http://localhost:{somePort}
```

the door will be opened automatically by the application